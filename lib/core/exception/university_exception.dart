class UniversityException implements Exception {
  final String message;

  UniversityException({required this.message});
}
