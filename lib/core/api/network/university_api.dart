import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:prueba_tyba/core/api/repository/university_repository.dart';
import 'package:prueba_tyba/core/exception/university_exception.dart';
import 'package:prueba_tyba/core/models/university_model.dart';

class UniversityApi extends UniversityRepository {
  final Dio dio;
  final _urlBase = dotenv.env['URL_BASE'];

  UniversityApi({required this.dio});

  @override
  Future<List<UniversityModel>> getAllUniversity() async {
    final listOfResults = <UniversityModel>[];
    try {
      var result = await dio.get('$_urlBase');
      if (result.data != null) {
        for (var university in result.data) {
          listOfResults.add(UniversityModel.fromJson(university));
        }
      }
    } catch (e) {
      throw UniversityException(message: 'Opps... algo salio mal : $e');
    }
    return listOfResults;
  }
}
