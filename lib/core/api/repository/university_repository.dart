import 'package:prueba_tyba/core/models/university_model.dart';

abstract class UniversityRepository {
  Future<List<UniversityModel>> getAllUniversity();
}
