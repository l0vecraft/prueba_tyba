import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:prueba_tyba/core/api/repository/university_repository.dart';
import 'package:prueba_tyba/core/exception/university_exception.dart';
import 'package:prueba_tyba/core/models/university_model.dart';
import 'package:prueba_tyba/locator.dart';

class UniversityProvider extends ChangeNotifier {
  final _api = l.get<UniversityRepository>();

  Future<List<UniversityModel>> getAllUniversities() async {
    try {
      var res = await _api.getAllUniversity();
      return res;
    } on UniversityException catch (e) {
      throw UniversityException(message: e.message);
    }
  }
}

final universityProvider =
    ChangeNotifierProvider<UniversityProvider>((ref) => UniversityProvider());
final universityFuture = FutureProvider(
    (ref) async => await ref.watch(universityProvider).getAllUniversities());
