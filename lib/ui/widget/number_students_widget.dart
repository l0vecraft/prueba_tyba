import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NumberOfStudentsWidget extends StatelessWidget {
  const NumberOfStudentsWidget({
    Key? key,
    required String numberStudents,
  })  : _numberStudents = numberStudents,
        super(key: key);

  final String _numberStudents;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      alignment: Alignment.center,
      height: _numberStudents.isEmpty ? 0 : 100.h,
      duration: const Duration(milliseconds: 400),
      child: Text("Numero de estudiantes: $_numberStudents",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.sp)),
    );
  }
}
