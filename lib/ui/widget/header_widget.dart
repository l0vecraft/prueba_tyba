import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tyba/ui/style/style.dart';

class HeaderWidget extends StatelessWidget {
  final String header;
  final String text;
  const HeaderWidget({
    Key? key,
    required this.header,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '$header: ',
              style: AppStyle.header1,
            ),
            Expanded(
              child: Text(
                text,
                style: AppStyle.title1,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      );
}
