import 'package:flutter/material.dart';
import 'package:prueba_tyba/core/models/university_model.dart';

class UniversitiListView extends StatelessWidget {
  final List<UniversityModel> universities;
  const UniversitiListView({
    Key? key,
    required this.universities,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: universities.length,
      itemBuilder: (context, index) => ListTile(
        title: Text('${universities[index].name}'),
        subtitle: Text("${universities[index].country}"),
        onTap: () => Navigator.pushNamed(context, 'details/',
            arguments: universities[index]),
      ),
    );
  }
}
