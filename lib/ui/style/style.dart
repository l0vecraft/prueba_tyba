import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyle {
  static TextStyle title1 = TextStyle(fontSize: 20.sp);
  static TextStyle header1 =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 20.sp);
}
