import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:prueba_tyba/core/models/university_model.dart';
import 'package:prueba_tyba/core/validators/form_validator.dart';
import 'package:prueba_tyba/ui/style/style.dart';
import 'package:prueba_tyba/ui/widget/header_widget.dart';
import 'package:prueba_tyba/ui/widget/number_students_widget.dart';

class DetailScreen extends ConsumerStatefulWidget {
  final UniversityModel university;
  const DetailScreen({super.key, required this.university});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _DetailScreenState();
}

class _DetailScreenState extends ConsumerState<DetailScreen> {
  XFile? _imageSelected;
  final _imagePicker = ImagePicker();
  final _controller = TextEditingController();
  final _globalKey = GlobalKey<FormState>();
  var _numberStudents = '';

  void _pickImage() async {
    try {
      var imageFile = await _imagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _imageSelected = imageFile;
        widget.university.image = _imageSelected!.path;
      });
      log(_imageSelected!.path);
    } catch (e) {
      log(e.toString());
    }
  }

  void _formValidation() {
    if (!_globalKey.currentState!.validate()) {
      return;
    } else {
      _globalKey.currentState!.save();
      setState(() => _numberStudents = _controller.text);
    }
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail screen'),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: CustomScrollView(
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate([
                    InkWell(
                      onTap: _pickImage,
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.sp),
                        child: Container(
                          width: double.maxFinite,
                          alignment: Alignment.center,
                          child: Stack(
                            children: [
                              CircleAvatar(
                                radius: 60.sp,
                                child: widget.university.image == null
                                    ? Text(
                                        widget.university.name![0],
                                        style: TextStyle(fontSize: 30.sp),
                                      )
                                    : Image.file(File(_imageSelected!.path)),
                              ),
                              Visibility(
                                visible: widget.university.image == null,
                                child: Positioned(
                                    bottom: 5,
                                    right: 18,
                                    child: Icon(
                                      Icons.edit,
                                      size: 30.sp,
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    HeaderWidget(header: 'Name', text: widget.university.name!),
                    HeaderWidget(
                        header: 'Code', text: widget.university.alphaTwoCode!),
                    HeaderWidget(
                        header: 'Country', text: widget.university.country!),
                    Wrap(
                      children: widget.university.domains!
                          .map((e) => Text(e, style: AppStyle.title1))
                          .toList(),
                    ),
                    Form(
                        key: _globalKey,
                        child: Container(
                          height: 200.h,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                  flex: 2,
                                  child: TextFormField(
                                    controller: _controller,
                                    onSaved: (value) => _formValidation,
                                    validator: (value) =>
                                        FormValidator.isEmpty(value!),
                                    inputFormatters: [
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    maxLength: 6,
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        hintText: 'Numero de estudiantes',
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(12))),
                                  )),
                              Flexible(
                                child: ElevatedButton(
                                    onPressed: _formValidation,
                                    child: Text('Submit')),
                              )
                            ],
                          ),
                        )),
                    NumberOfStudentsWidget(numberStudents: _numberStudents)
                  ]),
                ),
              ],
            )),
      ),
    );
  }
}
