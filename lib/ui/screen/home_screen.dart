import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_tyba/core/models/university_model.dart';
import 'package:prueba_tyba/core/providers/university_provider.dart';
import 'package:prueba_tyba/ui/widget/university_grid_view.dart';
import 'package:prueba_tyba/ui/widget/university_list_view.dart';

final changeViewProvider = StateProvider<bool>((ref) => false);

class HomeScreen extends ConsumerStatefulWidget {
  const HomeScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends ConsumerState<HomeScreen> {
  final _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    var univeristyData = ref.watch(universityFuture);
    var changeView = ref.watch(changeViewProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Prueba tecnica tyba'),
        actions: [
          Switch(
              value: changeView,
              thumbColor: MaterialStateProperty.all(Colors.amber),
              activeColor: Colors.amber,
              onChanged: (value) =>
                  ref.read(changeViewProvider.notifier).state = value)
        ],
      ),
      body: Container(
        child: Scrollbar(
          controller: _scrollController,
          thickness: 5,
          child: univeristyData.when(
              data: (university) => changeView
                  ? UniversityGridView(
                      universities: university,
                    )
                  : UniversitiListView(universities: university),
              error: (error, s) => Center(
                    child: Text('$error'),
                  ),
              loading: () => const Center(
                    child: CircularProgressIndicator(),
                  )),
        ),
      ),
    );
  }
}
