import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:prueba_tyba/core/api/network/university_api.dart';
import 'package:prueba_tyba/core/api/repository/university_repository.dart';

final l = GetIt.instance;
final _dio = Dio();

void setUpLocator() {
  l.registerLazySingleton<UniversityRepository>(() => UniversityApi(dio: _dio));
}
