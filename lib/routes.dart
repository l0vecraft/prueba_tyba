import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prueba_tyba/core/models/university_model.dart';
import 'package:prueba_tyba/ui/screen/detail_screen.dart';
import 'package:prueba_tyba/ui/screen/home_screen.dart';

class Routes {
  static Route<dynamic> onGenerateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (context) => const HomeScreen());
      case 'details/':
        var params = settings.arguments as UniversityModel;
        return MaterialPageRoute(
            builder: (context) => DetailScreen(university: params));
      default:
        return MaterialPageRoute(builder: (context) => const HomeScreen());
    }
  }
}
