# Prueba tecnica de Tyba

aqui se encontraran las instrucciones para poder ejecutarlo de forma correcta.

se esta utilizando **json_annotacion** por lo que en caso de que se den errores de archivos generados deberia ejecutarse este comando en el direcotrio raiz
`flutter pub run build_runner build`

por otro lado, tambien se esta utilizando la libreria **flutter_dotenv** por tanto el archivo **.env** hara falta, se recomienda crearlo en la raiz del proyecto y agregar lo siguiente
`URL_BASE = 'https://tyba-assets.s3.amazonaws.com/FE-Engineer-test/universities.json'`
